# Akka Sample App

Simple CRUD application using Akka Http, Slick, PostgreSQL, Angular.

### Usage

With Docker: `docker-compose up`

Open [http://localhost:9000](http://localhost:9000)
