export const environment = {
  production: true,
  apiUrl: API_URL || 'http://localhost:8080/api',
};
