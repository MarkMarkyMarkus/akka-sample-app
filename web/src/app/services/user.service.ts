import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from '../model';
import {environment} from "../../environments/environment";

const baseUrl: string = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${baseUrl}/users`);
  }

  get(id: number): Observable<IUser> {
    return this.http.get<IUser>(`${baseUrl}/user/${id}`);
  }

  create(data: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${baseUrl}/users`, data);
  }

  update(id: number, data: IUser): Observable<IUser> {
    return this.http.put<IUser>(`${baseUrl}/user/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/user/${id}`);
  }
}
