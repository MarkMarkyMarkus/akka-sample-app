import {Component, OnInit} from '@angular/core';
import {UserService} from 'src/app/services/user.service';
import {IUser, User} from "../../model";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {

  user: IUser = new User(-1, '', '', null, '');
  submitted: boolean = false;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
  }

  saveUser(): void {
    const data: IUser = new User(
      this.user.id,
      this.user.firstName,
      this.user.lastName,
      this.user.birthDay,
      this.user.address,
    );

    this.userService.create(data)
      .subscribe(
        () => {
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newUser(): void {
    this.submitted = false;
    this.user = new User(-1, '', '', null, '');
  }
}
