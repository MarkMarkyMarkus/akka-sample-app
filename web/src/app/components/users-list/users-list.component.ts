import {Component, OnInit} from '@angular/core';
import {UserService} from 'src/app/services/user.service';
import {IUser} from "../../model";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  users: IUser[];
  currentUser: IUser = null;
  currentIndex: number = -1;
  title: string = '';

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.retrieveUsers();
  }

  retrieveUsers(): void {
    this.userService.getAll()
      .subscribe(
        data => {
          this.users = data;
        },
        error => {
          console.log(error);
        });
  }

  setActiveUser(user: IUser, index: number): void {
    this.currentUser = user;
    this.currentIndex = index;
  }
}
