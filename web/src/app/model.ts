export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  birthDay: Date;
  address: string;
}

export class User implements IUser {
  id: number;
  firstName: string;
  lastName: string;
  birthDay: Date;
  address: string;

  constructor(id: number, firstName: string, lastName: string, birthDay: Date, address: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDay = birthDay;
    this.address = address;
  }
}
