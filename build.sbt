name := "SampleApp"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaVersion     = "2.6.9"
val AkkaHttpVersion = "10.2.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "ch.megard" %% "akka-http-cors" % "1.1.0",

  "com.github.pureconfig" %% "pureconfig" % "0.13.0",

  "com.typesafe.slick" %% "slick" % "3.3.3",
  "org.postgresql" % "postgresql" % "42.2.16",
  "com.zaxxer" % "HikariCP" % "3.4.5",

  "ch.qos.logback" % "logback-classic" % "1.2.3" % Runtime,

  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % Test,
  "org.scalamock" %% "scalamock" % "5.0.0" % Test,
  "com.h2database" % "h2" % "1.4.200" % Test
)
