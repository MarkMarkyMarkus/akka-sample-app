package routes

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import domain.UserService
import org.scalamock.scalatest.MockFactory
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class HttpRouteTest
  extends AnyWordSpec
    with Matchers
    with ScalatestRouteTest
    with MockFactory {

  val mockUserService: UserService = mock[UserService]
  val route          : Route       = new HttpRoute(mockUserService).route

  "HttpRouter" should {

    "handle GET requests to /api" in {
      Get("/api") ~> Route.seal(route) ~> check {
        handled shouldBe true
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get() ~> route ~> check {
        handled shouldBe false
      }
    }
  }
}
