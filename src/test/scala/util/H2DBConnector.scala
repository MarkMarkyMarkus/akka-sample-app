package util

object H2DBConnector extends DatabaseConnector {

  val profile = slick.jdbc.H2Profile

  import profile.api._

  val db: profile.backend.DatabaseDef = Database.forURL(
    url = "jdbc:h2:mem:test",
    driver = "org.h2.Driver",
    keepAliveConnection = true)

  db.createSession()
}
