package domain

import java.time.LocalDate

import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import slick.dbio.DBIO
import util.{H2DBConnector, Runner, StorageRunner}

class UserServiceImplTest
  extends AsyncWordSpec
    with Matchers
    with AsyncMockFactory {

  val mockUserStorage: UserStorage     = mock[UserStorage]
  val runner         : Runner          = new StorageRunner(H2DBConnector)
  val userService    : UserServiceImpl = new UserServiceImpl(runner, mockUserStorage)
  val user           : User            = User(0, "Test", "Testovich", LocalDate.now(), "Saint-P")

  "getUsers" should {
    "return all users" in {

      (mockUserStorage.getUsers _).expects() returning DBIO.successful(List(user))

      userService.getUsers map { users =>
        users shouldBe Seq(user)
      }
    }
  }

  "create" should {
    "return new user" in {

      (mockUserStorage.saveUser _).expects(user) returning DBIO.successful(user.copy(id = 1))

      userService.create(user) map { newUser =>
        newUser shouldBe user.copy(id = 1)
      }
    }
  }

  "getUser" should {
    "return current user" in {

      (mockUserStorage.getUser _).expects(user.id) returning DBIO.successful(Some(user))

      userService.getUser(user.id) map { currentUser =>
        currentUser shouldBe Some(user)
      }
    }
  }

  "updateUser" should {
    "return updated user" in {

      (mockUserStorage.updateUser _).expects(user) returning DBIO.successful(Some(user))

      userService.updateUser(user) map { updatedUser =>
        updatedUser shouldBe Some(user)
      }
    }
  }

  "deleteUser" should {
    "return int value" in {

      (mockUserStorage.deleteUser _).expects(user.id) returning DBIO.successful(1)

      userService.deleteUser(user.id) map { removingResult =>
        removingResult shouldBe 1
      }
    }
  }
}
