package domain

import java.time.LocalDate

import akka.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import domain.UserJsonProtocol._
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json.enrichAny

import scala.concurrent.Future

class UserRouteTest
  extends AnyWordSpec
    with Matchers
    with ScalaFutures
    with ScalatestRouteTest
    with MockFactory {

  val mockUserService: UserService = mock[UserService]
  val route          : Route       = new UserRoute(mockUserService).route
  val user           : User        = User(0L, "Test", "Testovich", LocalDate.now(), "Saint-P")

  "UserRouter" should {

    "return empty list for GET request to /users" in {

      (mockUserService.getUsers _).expects().returning(Future(Seq.empty[User])).once()

      Get("/users") ~> route ~> check {
        responseAs[List[User]] shouldEqual List.empty[User]
      }
    }

    "return user for POST request to /users" in {

      (mockUserService.create _).expects(user).returning(Future(user.copy(id = 1))).once()

      Post("/users", HttpEntity(MediaTypes.`application/json`, user.toJson.toString)) ~> route ~> check {
        responseAs[User] shouldEqual user.copy(id = 1)
      }
    }

    "return user for GET request to /user/{id}" in {

      (mockUserService.getUser _).expects(user.id).returning(Future(Some(user))).once()

      Get(s"/user/${user.id}") ~> route ~> check {
        responseAs[User] shouldEqual user
      }
    }

    "return updated user for PUT request to /user/{id}" in {

      (mockUserService.updateUser _).expects(user).returning(Future(Some(user))).once()

      Put(s"/user/${user.id}", HttpEntity(MediaTypes.`application/json`, user.toJson.toString)) ~> route ~> check {
        responseAs[User] shouldEqual user
      }
    }

    "return NoContent (code: 204) for DELETE requests to /user/{id}" in {

      (mockUserService.deleteUser _).expects(user.id).returning(Future(1)).once()

      Delete(s"/user/${user.id}") ~> route ~> check {
        status shouldEqual StatusCodes.NoContent
      }
    }
  }
}
