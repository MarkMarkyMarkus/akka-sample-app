package domain

import java.time.LocalDate

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import util.{H2DBConnector, Runner, StorageRunner}

class JdbcUserStorageTest
  extends AsyncWordSpec
    with Matchers {

  val runner     : Runner      = new StorageRunner(H2DBConnector)
  val userStorage: UserStorage = new JdbcUserStorage
  runner.run(userStorage.init)

  val user: User = User(0, "Test", "Testovich", LocalDate.now(), "Saint-P")

  "getUsers" should {
    "return all users" in {
      runner.run(userStorage.getUsers) map { users =>
        users shouldBe Seq()
      }
    }
  }

  "create" should {
    "return new user" in {
      runner.run(userStorage.saveUser(user)) map { newUser =>
        newUser shouldBe newUser.copy(id = 1)
      }
    }
  }

  "getUser" should {
    "return current user" in {
      runner.run(userStorage.getUser(1)) map { currentUser =>
        currentUser shouldBe Some(user.copy(id = 1))
      }
    }
  }

  "updateUser" should {
    "return updated user" in {
      runner.run(userStorage.getUser(1)) flatMap { firstUser =>
        runner.run(userStorage.updateUser(user.copy(id = firstUser.get.id, firstName = "Other name")))
      } map { updatedUser =>
        updatedUser shouldBe Some(user.copy(id = 1, firstName = "Other name"))
      }
    }
  }

  "deleteUser" should {
    "return int value" in {
      runner.run(userStorage.deleteUser(1)) map { removingResult =>
        removingResult shouldBe 1
      }
    }
  }
}
