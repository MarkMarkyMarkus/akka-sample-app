package util

import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

/**
 * DBIO actions runner.
 * Runs actions in a transaction or without it.
 */
trait Runner {
  def run[T](actions: DBIO[T]): Future[T]

  def runInTransaction[T](action: DBIO[T]): Future[T]
}

/**
 * Storage DBIO actions runner.
 *
 * @param databaseConnector connector to the db
 */
class StorageRunner(val databaseConnector: DatabaseConnector) extends Runner {

  import databaseConnector._

  def run[T](actions: DBIO[T]): Future[T] = db.run(actions)

  def runInTransaction[T](action: DBIO[T]): Future[T] =
    db.run(action.transactionally)
}
