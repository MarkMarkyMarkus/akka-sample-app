package util

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.JdbcProfile

/**
 * Database connector. Handles all db communications.
 */
trait DatabaseConnector {
  val profile: JdbcProfile
  val db     : profile.api.Database
}

/**
 * Connector to PostgreSQL using Hikari connection pooling.
 *
 * @param jdbcURL    db url
 * @param dbUser     db user
 * @param dbPassword db password
 */
class PostgresDBConnector(jdbcURL: String, dbUser: String, dbPassword: String)
  extends DatabaseConnector {

  private val hikariDataSource = {
    val hikariConfig = new HikariConfig()
    hikariConfig.setJdbcUrl(jdbcURL)
    hikariConfig.setUsername(dbUser)
    hikariConfig.setPassword(dbPassword)

    new HikariDataSource(hikariConfig)
  }

  val profile = slick.jdbc.PostgresProfile

  import profile.api._

  val db: profile.backend.DatabaseDef = Database.forDataSource(hikariDataSource, None)
  db.createSession()
}
