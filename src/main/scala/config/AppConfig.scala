package config

import pureconfig.ConfigSource
import pureconfig.generic.auto._

/**
 * Application config.
 */
object AppConfig {
  def load(): AppConfig =
    ConfigSource.default.load[AppConfig] match {
      case Right(config) => config
      case Left(error)   =>
        throw new RuntimeException(s"Cannot load config file: $error")
    }
}

case class AppConfig(http: HttpConfig,
                     database: DatabaseConfig)

case class HttpConfig(host: String, port: Int)

case class DatabaseConfig(host: String,
                          port: String,
                          name: String,
                          username: String,
                          password: String)
