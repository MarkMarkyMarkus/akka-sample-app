import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import config.AppConfig
import domain.{JdbcUserStorage, UserServiceImpl}
import routes.HttpRoute
import util.{PostgresDBConnector, StorageRunner}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor}

/**
 * Application entrypoint.
 */
object Main extends App {
  def startApplication(): Unit = {
    implicit val system          : ActorSystem[Nothing]     = ActorSystem(Behaviors.empty, "my-system")
    implicit val executionContext: ExecutionContextExecutor = system.executionContext

    val config            = AppConfig.load()
    val jdbcURL           =
      s"jdbc:postgresql://${config.database.host}:${config.database.port}/${config.database.name}"
    val databaseConnector =
      new PostgresDBConnector(jdbcURL, config.database.username, config.database.password)
    val userStorage       = new JdbcUserStorage()
    val storageRunner     = new StorageRunner(databaseConnector)
    val userService       = new UserServiceImpl(storageRunner, userStorage)

    /** Not a very good way to initialize the storage.
     * TODO: implement more appropriate migration approach. */
    userService.initStorage

    val httpRoute = new HttpRoute(userService)

    Http().newServerAt(config.http.host, config.http.port).bind(httpRoute.route)

    scala.sys.addShutdownHook {
      system.terminate()
      Await.result(system.whenTerminated, 10.seconds)
    }
  }

  startApplication()
}
