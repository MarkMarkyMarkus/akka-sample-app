package routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import domain.{UserRoute, UserService}

import scala.concurrent.ExecutionContext

/**
 * Root route.
 *
 * @param userService      user service
 * @param executionContext execution context
 */
class HttpRoute(userService: UserService)(implicit executionContext: ExecutionContext) {
  private val usersRouter = new UserRoute(userService)

  val route: Route =
    pathPrefix("api") {
      usersRouter.route
    }
}
