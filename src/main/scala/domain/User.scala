package domain

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}

import scala.util.Try

/**
 * Application user.
 *
 * @param id        user's identifier
 * @param firstName user's first name
 * @param lastName  user's last name
 * @param birthDay  user's date of birth
 * @param address   user's address
 */
case class User(id: Long,
                firstName: String,
                lastName: String,
                birthDay: LocalDate,
                address: String) {
  require(firstName.nonEmpty, "Empty firstName")
  require(lastName.nonEmpty, "Empty lastName")
  require(address.nonEmpty, "Empty address")
}

/**
 * Json protocol for [[domain.User]].
 */
object UserJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  /**
   * Date formatter for [[UserJsonProtocol]].
   * Implements (un)marshalling LocalDate <-> String.
   */
  implicit object DateFormat extends JsonFormat[LocalDate] {
    def write(date: LocalDate): JsString = JsString(dateToIsoString(date))

    def read(json: JsValue): LocalDate = json match {
      case JsString(rawDate) =>
        parseIsoDateString(rawDate)
          .fold(deserializationError(s"Unexpected type $rawDate"))(identity)
      case error             => deserializationError(s"Expected JsString, got $error")
    }
  }

  implicit val userFormat: RootJsonFormat[User] = jsonFormat5(User)

  private def dateToIsoString(date: LocalDate): String =
    date.atStartOfDay().format(DateTimeFormatter.ISO_DATE_TIME)

  private def parseIsoDateString(date: String): Option[LocalDate] =
    Try {
      LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME).toLocalDate
    }.toOption
}
