package domain

import util.Runner

import scala.concurrent.{ExecutionContext, Future}

/**
 * Simple user service.
 */
trait UserService {
  def getUsers: Future[Seq[User]]

  def getUser(userId: Long): Future[Option[User]]

  def create(user: User): Future[User]

  def updateUser(user: User): Future[Option[User]]

  def deleteUser(userId: Long): Future[Int]
}

/**
 * Implements [[UserService]].
 *
 * @param storageRunner    runner for DB actions
 * @param userStorage      users storage
 * @param executionContext execution context
 */
class UserServiceImpl(storageRunner: Runner,
                      userStorage: UserStorage)
                     (implicit executionContext: ExecutionContext)
  extends UserService {

  def initStorage: Future[Unit] = storageRunner.run(userStorage.init)

  def getUsers: Future[Seq[User]] =
    storageRunner.run(userStorage.getUsers)

  def getUser(userId: Long): Future[Option[User]] =
    storageRunner.run(userStorage.getUser(userId))

  def create(user: User): Future[User] =
    storageRunner.runInTransaction(userStorage.saveUser(user))

  def updateUser(user: User): Future[Option[User]] =
    storageRunner.runInTransaction(userStorage.updateUser(user))

  def deleteUser(userId: Long): Future[Int] =
    storageRunner.runInTransaction(userStorage.deleteUser(userId))
}
