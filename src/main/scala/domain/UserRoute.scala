package domain

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive, ExceptionHandler, RejectionHandler, Route}
import org.slf4j.{Logger, LoggerFactory}
import spray.json.enrichAny

import scala.concurrent.ExecutionContext

/**
 * User router.
 * Simple CRUD API implementation.
 *
 * @param usersService     user service
 * @param executionContext execution context
 */
class UserRoute(usersService: UserService)(
  implicit executionContext: ExecutionContext) {

  import UserJsonProtocol._
  import StatusCodes._
  import ch.megard.akka.http.cors.scaladsl.CorsDirectives.{cors, corsRejectionHandler}
  import usersService._

  val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  /**
   * CORS filter rejection and exception handlers.
   */
  lazy val rejectionHandler: RejectionHandler = corsRejectionHandler.withFallback(RejectionHandler.default)
  lazy val exceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: Exception =>
      log.error("Error: {}", e)
      complete(BadRequest -> e.getMessage)
  }
  lazy val handleErrors    : Directive[Unit]  =
    handleRejections(rejectionHandler) & handleExceptions(exceptionHandler)

  lazy val route: Route =
    cors() {
      handleErrors {
        concat(
          path("users")(usersRoute),
          path("user" / LongNumber)(userRoute),
        )
      }
    }

  lazy val usersRoute: Route =
    concat(
      get {
        onSuccess(getUsers) { users =>
          complete(OK -> users.toJson)
        }
      },

      post {
        entity(as[User]) { user =>
          onSuccess(create(user)) {
            user => complete(OK -> user.toJson)
          }
        }
      }
    )

  /**
   * GET    -> returns the user if any.
   * DELETE -> always returns NoContent even if no user is found.
   * PUT    -> returns the updated user if any.
   *
   * @param userId user's identifier
   * @return user's subroute
   */
  def userRoute(userId: Long): Route = {
    concat(
      get {
        onSuccess(getUser(userId)) {
          case Some(user) => complete(OK -> user.toJson)
          case None       =>
            complete(BadRequest -> None)
        }
      },

      delete {
        onSuccess(deleteUser(userId)) {
          _ => complete(NoContent)
        }
      },

      put {
        entity(as[User]) { user =>
          onSuccess(updateUser(user)) {
            case Some(user) => complete(OK -> user.toJson)
            case None       =>
              complete(BadRequest -> None)
          }
        }
      },
    )
  }
}
