package domain

import slick.dbio.{DBIO, Effect}
import slick.jdbc.PostgresProfile.api._
import slick.sql.FixedSqlAction

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Simple [[domain.User]] storage.
 */
trait UserStorage {
  def init: FixedSqlAction[Unit, NoStream, Effect.Schema]

  def getUsers: DBIO[Seq[User]]

  def getUser(userId: Long): DBIO[Option[User]]

  def saveUser(user: User): DBIO[User]

  def updateUser(user: User): DBIO[Option[User]]

  def deleteUser(userId: Long): DBIO[Int]
}

/**
 * Implements [[UserStorage]] using Slick.
 */
class JdbcUserStorage
  extends UserTable
    with UserStorage {

  def init: FixedSqlAction[Unit, NoStream, Effect.Schema] =
    users.schema.createIfNotExists

  def getUsers: DBIO[Seq[User]] = users.result

  def getUser(userId: Long): DBIO[Option[User]] =
    users.filter(_.id === userId).result.headOption

  def saveUser(user: User): DBIO[User] =
    (users returning users) += user

  def updateUser(user: User): DBIO[Option[User]] = {
    users.filter(_.id === user.id).update(user).map {
      case 0 => None
      case _ => Some(user)
    }
  }

  def deleteUser(userId: Long): DBIO[Int] =
    users.filter(_.id === userId).delete
}
