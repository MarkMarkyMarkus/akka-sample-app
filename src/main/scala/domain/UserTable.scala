package domain

import java.sql.Date
import java.time.LocalDate

import slick.ast.BaseTypedType
import slick.jdbc.JdbcType
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery

/**
 * Representation of [[User]] in db.
 */
trait UserTable {

  class Users(tag: Tag) extends Table[User](tag, "users") {

    /**
     * Mapper java.time.LocalDate <-> java.sql.Date
     *
     * @return mapped value
     */
    implicit def dateMapper: JdbcType[LocalDate] with BaseTypedType[LocalDate] =
      MappedColumnType.base[LocalDate, Date](
        localDate => Date.valueOf(localDate),
        date => date.toLocalDate
      )

    def id = column[Long]("id", O.AutoInc, O.PrimaryKey)

    def firstName = column[String]("first_name")

    def lastName = column[String]("last_name")

    def birthDay = column[LocalDate]("date_of_birth")

    def address = column[String]("address")

    def * =
      (id, firstName, lastName, birthDay, address) <> ((User.apply _).tupled, User.unapply)
  }

  protected val users = TableQuery[Users]
}
