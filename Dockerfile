FROM hseeberger/scala-sbt:8u222_1.3.5_2.13.1
ARG SBT_VERSION="1.3.13"

WORKDIR /app
ADD . /app

CMD ["sbt", "run"]
